# viaspf-record

The **viaspf-record** library contains a data model and parser for SPF records.
SPF records are described in the *Sender Policy Framework* specification,
version 1 ([RFC 7208]).

The data structures in this library constitute a complete and faithful encoding
of the ABNF in [RFC 7208, section 12]. Extensive checking ensures correctness
and conformance with the specification.

This library is used in [viaspf], where you can find a complete implementation
of SPF, including APIs for performing SPF queries. The viaspf-record library is
a stand-alone product and could also be used in other projects.

The minimum supported Rust version is 1.56.1.

[RFC 7208]: https://www.rfc-editor.org/rfc/rfc7208
[RFC 7208, section 12]: https://www.rfc-editor.org/rfc/rfc7208#section-12
[viaspf]: https://crates.io/crates/viaspf

## Usage

This is a Rust library. Include viaspf-record in `Cargo.toml` as usual.

The struct [`Record`] represents a syntactically valid SPF record. A `Record`
can be constructed programmatically or parsed from a string.

```rust
use std::net::Ipv4Addr;
use viaspf_record::*;

let record = "v=spf1 mx ip4:12.34.56.78/24 -all".parse();

assert_eq!(
    record,
    Ok(Record {
        terms: vec![
            Term::Directive(Directive {
                qualifier: None,
                mechanism: Mechanism::Mx(Mx {
                    domain_spec: None,
                    prefix_len: None,
                }),
            }),
            Term::Directive(Directive {
                qualifier: None,
                mechanism: Mechanism::Ip4(Ip4 {
                    addr: Ipv4Addr::new(12, 34, 56, 78),
                    prefix_len: Some(Ip4CidrLength::new(24).unwrap()),
                }),
            }),
            Term::Directive(Directive {
                qualifier: Some(Qualifier::Fail),
                mechanism: Mechanism::All,
            }),
        ],
    })
);
```

Refer to the [API documentation] for details.

[`Record`]: https://docs.rs/viaspf-record/0.4.1/viaspf_record/struct.Record.html
[API documentation]: https://docs.rs/viaspf-record

## Licence

Copyright © 2020–2023 David Bürgin

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see https://www.gnu.org/licenses/.
