// viaspf – implementation of the SPF specification
// Copyright © 2020–2023 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

use crate::{
    eval::{
        query::{Query, Resolver},
        EvalError, EvalResult, Evaluate, EvaluateMatch, EvaluateToString, MatchResult,
    },
    lookup::{self, LookupError, LookupResult, Name},
    record::{
        DomainSpec, DualCidrLength, Exists, ExplainString, Explanation, Include, Ip4, Ip6,
        Mechanism, Mx, Ptr, Redirect, A,
    },
    result::{ErrorCause, SpfResult},
    trace::Tracepoint,
};
use async_trait::async_trait;
use std::{
    error::Error,
    fmt::{self, Display, Formatter},
    net::IpAddr,
};

#[async_trait]
impl EvaluateMatch for Mechanism {
    async fn evaluate_match(
        &self,
        query: &mut Query<'_>,
        resolver: &Resolver<'_>,
    ) -> EvalResult<MatchResult> {
        trace!(query, Tracepoint::EvaluateMechanism(self.clone()));

        let mechanism: &(dyn EvaluateMatch + Send + Sync) = match self {
            Self::All => return Ok(MatchResult::Match),
            Self::Include(include) => include,
            Self::A(a) => a,
            Self::Mx(mx) => mx,
            Self::Ptr(ptr) => ptr,
            Self::Ip4(ip4) => ip4,
            Self::Ip6(ip6) => ip6,
            Self::Exists(exists) => exists,
        };

        mechanism.evaluate_match(query, resolver).await
    }
}

#[async_trait]
impl EvaluateMatch for Include {
    async fn evaluate_match(
        &self,
        query: &mut Query<'_>,
        resolver: &Resolver<'_>,
    ) -> EvalResult<MatchResult> {
        increment_lookup_count(query)?;

        let target_name = get_target_name(&self.domain_spec, query, resolver).await?;
        trace!(query, Tracepoint::TargetName(target_name.clone()));

        let result = execute_recursive_query(query, resolver, target_name, true).await;

        // See the table in §5.2.
        use SpfResult::*;
        match result {
            Pass => Ok(MatchResult::Match),
            Fail(_) | Softfail | Neutral => Ok(MatchResult::NoMatch),
            Temperror => Err(EvalError::RecursiveTemperror),
            Permerror => Err(EvalError::RecursivePermerror),
            None => Err(EvalError::IncludeNoRecord),
        }
    }
}

#[async_trait]
impl EvaluateMatch for A {
    async fn evaluate_match(
        &self,
        query: &mut Query<'_>,
        resolver: &Resolver<'_>,
    ) -> EvalResult<MatchResult> {
        increment_lookup_count(query)?;

        let target_name =
            get_target_name_or_domain(self.domain_spec.as_ref(), query, resolver).await?;
        trace!(query, Tracepoint::TargetName(target_name.clone()));

        let ip = query.params.ip();

        // §5.3: ‘An address lookup is done on the <target-name> using the type
        // of lookup (A or AAAA) appropriate for the connection type (IPv4 or
        // IPv6).’
        let addrs = to_eval_result(resolver.lookup_a_or_aaaa(query, &target_name, ip).await)?;
        increment_void_lookup_count_if_void(query, addrs.len())?;

        for addr in addrs {
            trace!(query, Tracepoint::TryIpAddr(addr));

            // ‘The <ip> is compared to the returned address(es). If any address
            // matches, the mechanism matches.’
            if is_in_network(addr, self.prefix_len, ip) {
                return Ok(MatchResult::Match);
            }
        }

        Ok(MatchResult::NoMatch)
    }
}

#[async_trait]
impl EvaluateMatch for Mx {
    async fn evaluate_match(
        &self,
        query: &mut Query<'_>,
        resolver: &Resolver<'_>,
    ) -> EvalResult<MatchResult> {
        increment_lookup_count(query)?;

        let target_name =
            get_target_name_or_domain(self.domain_spec.as_ref(), query, resolver).await?;
        trace!(query, Tracepoint::TargetName(target_name.clone()));

        let mxs = to_eval_result(resolver.lookup_mx(query, &target_name).await)?;
        increment_void_lookup_count_if_void(query, mxs.len())?;

        let ip = query.params.ip();

        let mut i = 0;

        for mx in mxs {
            trace!(query, Tracepoint::TryMxName(mx.clone()));

            // §4.6.4: ‘the evaluation of each "MX" record MUST NOT result in
            // querying more than 10 address records -- either "A" or "AAAA"
            // resource records. If this limit is exceeded, the "mx" mechanism
            // MUST produce a "permerror" result.’
            increment_per_mechanism_lookup_count(query, &mut i)?;

            let addrs = to_eval_result(resolver.lookup_a_or_aaaa(query, &mx, ip).await)?;
            increment_void_lookup_count_if_void(query, addrs.len())?;

            for addr in addrs {
                trace!(query, Tracepoint::TryIpAddr(addr));

                if is_in_network(addr, self.prefix_len, ip) {
                    return Ok(MatchResult::Match);
                }
            }
        }

        Ok(MatchResult::NoMatch)
    }
}

#[async_trait]
impl EvaluateMatch for Ptr {
    async fn evaluate_match(
        &self,
        query: &mut Query<'_>,
        resolver: &Resolver<'_>,
    ) -> EvalResult<MatchResult> {
        increment_lookup_count(query)?;

        let target_name =
            get_target_name_or_domain(self.domain_spec.as_ref(), query, resolver).await?;
        trace!(query, Tracepoint::TargetName(target_name.clone()));

        let ip = query.params.ip();

        let ptrs = match to_eval_result(resolver.lookup_ptr(query, ip).await) {
            Ok(ptrs) => ptrs,
            // §5.5: ‘If a DNS error occurs while doing the PTR RR lookup, then
            // this mechanism fails to match.’
            Err(e) => {
                trace!(query, Tracepoint::ReverseLookupError(e));
                return Ok(MatchResult::NoMatch);
            }
        };
        increment_void_lookup_count_if_void(query, ptrs.len())?;

        let validated_names = get_validated_domain_names(query, resolver, ip, ptrs).await?;

        // ‘Check all validated domain names to see if they either match the
        // <target-name> domain or are a subdomain of the <target-name> domain.
        // If any do, this mechanism matches.’
        for name in &validated_names {
            trace!(query, Tracepoint::TryValidatedName(name.clone()));
            if name == &target_name || name.is_subdomain_of(&target_name) {
                return Ok(MatchResult::Match);
            }
        }

        // ‘If no validated domain name can be found, or if none of the
        // validated domain names match or are a subdomain of the <target-name>,
        // this mechanism fails to match.’
        Ok(MatchResult::NoMatch)
    }
}

pub async fn get_validated_domain_names(
    query: &mut Query<'_>,
    resolver: &Resolver<'_>,
    ip: IpAddr,
    names: Vec<Name>,
) -> EvalResult<Vec<Name>> {
    let mut validated_names = Vec::new();

    let mut i = 0;

    // §5.5: ‘For each record returned, validate the domain name by looking up
    // its IP addresses.’
    for name in names {
        trace!(query, Tracepoint::ValidatePtrName(name.clone()));

        // §4.6.4: ‘the evaluation of each "PTR" record MUST NOT result in
        // querying more than 10 address records -- either "A" or "AAAA"
        // resource records. If this limit is exceeded, all records other than
        // the first 10 MUST be ignored.’
        if increment_per_mechanism_lookup_count(query, &mut i).is_err() {
            trace!(query, Tracepoint::PtrAddressLookupLimitExceeded);
            break;
        }

        let addrs = match to_eval_result(resolver.lookup_a_or_aaaa(query, &name, ip).await) {
            Ok(addrs) => addrs,
            // §5.5: ‘If a DNS error occurs while doing an A RR lookup, then
            // that domain name is skipped and the search continues.’
            Err(e) => {
                trace!(query, Tracepoint::PtrAddressLookupError(e));
                continue;
            }
        };
        increment_void_lookup_count_if_void(query, addrs.len())?;

        for addr in addrs {
            trace!(query, Tracepoint::TryIpAddr(addr));

            // §5.5: ‘If <ip> is among the returned IP addresses, then that
            // domain name is validated.’
            if addr == ip {
                trace!(query, Tracepoint::PtrNameValidated);
                validated_names.push(name);
                break;
            }
        }
    }

    Ok(validated_names)
}

#[async_trait]
impl EvaluateMatch for Ip4 {
    async fn evaluate_match(
        &self,
        query: &mut Query<'_>,
        _: &Resolver<'_>,
    ) -> EvalResult<MatchResult> {
        Ok(if is_in_network(self.addr, self.prefix_len, query.params.ip()) {
            MatchResult::Match
        } else {
            MatchResult::NoMatch
        })
    }
}

#[async_trait]
impl EvaluateMatch for Ip6 {
    async fn evaluate_match(
        &self,
        query: &mut Query<'_>,
        _: &Resolver<'_>,
    ) -> EvalResult<MatchResult> {
        Ok(if is_in_network(self.addr, self.prefix_len, query.params.ip()) {
            MatchResult::Match
        } else {
            MatchResult::NoMatch
        })
    }
}

fn is_in_network<A, L>(network_addr: A, prefix_len: Option<L>, ip: IpAddr) -> bool
where
    A: Into<IpAddr>,
    L: Into<DualCidrLength>,
{
    match (network_addr.into(), ip) {
        (IpAddr::V4(network_addr), IpAddr::V4(ip)) => {
            match prefix_len.and_then(|l| l.into().ip4()) {
                // §5: ‘If no CIDR prefix length is given in the directive, then
                // <ip> and the IP address are compared for equality.’
                None => network_addr == ip,
                // ‘If a CIDR prefix length is specified, then only the
                // specified number of high-order bits of <ip> and the IP
                // address are compared for equality.’
                Some(len) => {
                    let mask = u32::MAX << (32 - len.get());
                    (u32::from(network_addr) & mask) == (u32::from(ip) & mask)
                }
            }
        }
        (IpAddr::V6(network_addr), IpAddr::V6(ip)) => {
            match prefix_len.and_then(|l| l.into().ip6()) {
                None => network_addr == ip,
                Some(len) => {
                    let mask = u128::MAX << (128 - len.get());
                    (u128::from(network_addr) & mask) == (u128::from(ip) & mask)
                }
            }
        }
        _ => false,
    }
}

#[async_trait]
impl EvaluateMatch for Exists {
    async fn evaluate_match(
        &self,
        query: &mut Query<'_>,
        resolver: &Resolver<'_>,
    ) -> EvalResult<MatchResult> {
        increment_lookup_count(query)?;

        let target_name = get_target_name(&self.domain_spec, query, resolver).await?;
        trace!(query, Tracepoint::TargetName(target_name.clone()));

        // §5.7: ‘The resulting domain name is used for a DNS A RR lookup (even
        // when the connection type is IPv6).’
        let addrs = to_eval_result(resolver.lookup_a(query, &target_name).await)?;
        increment_void_lookup_count_if_void(query, addrs.len())?;

        // ‘If any A record is returned, this mechanism matches.’
        Ok(if addrs.is_empty() {
            MatchResult::NoMatch
        } else {
            MatchResult::Match
        })
    }
}

#[async_trait]
impl Evaluate for Redirect {
    async fn evaluate(&self, query: &mut Query<'_>, resolver: &Resolver<'_>) -> SpfResult {
        trace!(query, Tracepoint::EvaluateRedirect(self.clone()));

        if let Err(e) = increment_lookup_count(query) {
            trace!(query, Tracepoint::RedirectLookupLimitExceeded);
            query.result_cause = e.to_error_cause().map(From::from);
            return e.to_spf_result();
        }

        // §6.1: ‘if the <target-name> is malformed, the result is a "permerror"
        // rather than "none"’
        let target_name = match get_target_name(&self.domain_spec, query, resolver).await {
            Ok(n) => n,
            Err(e) => {
                trace!(query, Tracepoint::InvalidRedirectTargetName);
                query.result_cause = e.to_error_cause().map(From::from);
                return e.to_spf_result();
            }
        };
        trace!(query, Tracepoint::TargetName(target_name.clone()));

        let result = execute_recursive_query(query, resolver, target_name, false).await;

        // ‘The result of this new evaluation of check_host() is then considered
        // the result of the current evaluation with the exception that if no
        // SPF record is found, […] the result is a "permerror" rather than
        // "none".’
        match result {
            SpfResult::None => {
                trace!(query, Tracepoint::RedirectNoSpfRecord);
                query.result_cause = Some(ErrorCause::NoSpfRecord.into());
                SpfResult::Permerror
            }
            result => result,
        }
    }
}

async fn execute_recursive_query(
    query: &mut Query<'_>,
    resolver: &Resolver<'_>,
    target_name: Name,
    included: bool,
) -> SpfResult {
    // For recursive queries, adjust the target domain and included query flag
    // before execution, and restore them afterwards. Included redirections keep
    // their included flag set.
    let prev_name = query.params.replace_domain(target_name);
    let prev_included = query.state.is_included_query();
    query.state.set_included_query(prev_included || included);

    let result = query.execute(resolver).await;

    query.params.replace_domain(prev_name);
    query.state.set_included_query(prev_included);

    result
}

#[async_trait]
impl EvaluateToString for Explanation {
    async fn evaluate_to_string(
        &self,
        query: &mut Query<'_>,
        resolver: &Resolver<'_>,
    ) -> EvalResult<String> {
        trace!(query, Tracepoint::EvaluateExplanation(self.clone()));

        let target_name = get_target_name(&self.domain_spec, query, resolver).await?;
        trace!(query, Tracepoint::TargetName(target_name.clone()));

        // §6.2: ‘The fetched TXT record's strings are concatenated with no
        // spaces, and then treated as an explain-string, which is
        // macro-expanded.’
        let mut explain_string = match lookup_explain_string(resolver, query, &target_name).await {
            Ok(e) => e,
            Err(e) => {
                // ‘If there are any DNS processing errors (any RCODE other than
                // 0), or if no records are returned, or if more than one record
                // is returned, or if there are syntax errors in the explanation
                // string, then proceed as if no "exp" modifier was given.’
                use ExplainStringLookupError::*;
                trace!(
                    query,
                    match e {
                        DnsLookup(e) => Tracepoint::ExplainStringLookupError(e),
                        NoExplainString => Tracepoint::NoExplainString,
                        MultipleExplainStrings(s) => Tracepoint::MultipleExplainStrings(s),
                        Syntax(s) => Tracepoint::InvalidExplainStringSyntax(s),
                    }
                );

                // After the tracing above, may now conflate the error causes:
                return Err(EvalError::Dns(None));
            }
        };

        if let Some(f) = query.config.modify_exp_fn() {
            trace!(query, Tracepoint::ModifyExplainString(explain_string.clone()));
            f(&mut explain_string);
        }

        explain_string.evaluate_to_string(query, resolver).await
    }
}

#[derive(Debug)]
enum ExplainStringLookupError {
    DnsLookup(LookupError),
    NoExplainString,
    MultipleExplainStrings(Vec<String>),
    Syntax(String),
}

impl Error for ExplainStringLookupError {}

impl Display for ExplainStringLookupError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "failed to obtain explain string")
    }
}

impl From<LookupError> for ExplainStringLookupError {
    fn from(error: LookupError) -> Self {
        match error {
            LookupError::NoRecords => Self::NoExplainString,
            _ => Self::DnsLookup(error),
        }
    }
}

async fn lookup_explain_string(
    resolver: &Resolver<'_>,
    query: &mut Query<'_>,
    name: &Name,
) -> Result<ExplainString, ExplainStringLookupError> {
    let mut exps = resolver.lookup_txt(query, name).await?.into_iter();

    use ExplainStringLookupError::*;
    match exps.next() {
        None => Err(NoExplainString),
        Some(exp) => {
            let mut rest = exps.collect::<Vec<_>>();
            match *rest {
                [] => exp.parse().map_err(|_| Syntax(exp)),
                [..] => {
                    rest.insert(0, exp);
                    Err(MultipleExplainStrings(rest))
                }
            }
        }
    }
}

async fn get_target_name_or_domain(
    domain_spec: Option<&DomainSpec>,
    query: &mut Query<'_>,
    resolver: &Resolver<'_>,
) -> EvalResult<Name> {
    // §4.8: ‘For several mechanisms, the <domain-spec> is optional. If it is
    // not provided, the <domain> from the check_host() arguments is used as the
    // <target-name>.’
    match domain_spec {
        None => Ok(query.params.domain().clone()),
        Some(domain_spec) => get_target_name(domain_spec, query, resolver).await,
    }
}

async fn get_target_name(
    domain_spec: &DomainSpec,
    query: &mut Query<'_>,
    resolver: &Resolver<'_>,
) -> EvalResult<Name> {
    // §4.8: ‘The <domain-spec> string is subject to macro expansion […]. The
    // resulting string is the common presentation form of a fully qualified DNS
    // name’
    let mut name = domain_spec.evaluate_to_string(query, resolver).await?;
    truncate_target_name_string(&mut name, lookup::MAX_DOMAIN_LENGTH);
    Name::new(&name).map_err(|_| EvalError::InvalidName(name))
}

// §7.3: ‘When the result of macro expansion is used in a domain name query, if
// the expanded domain name exceeds 253 characters (the maximum length of a
// domain name in this format), the left side is truncated to fit, by removing
// successive domain labels (and their following dots) until the total length
// does not exceed 253 characters.’
fn truncate_target_name_string(s: &mut String, max: usize) {
    if s.ends_with('.') {
        s.pop();
    }
    let len = s.len();
    if len > max {
        if let Some((i, _)) = s
            .rmatch_indices('.')
            .take_while(|(i, _)| len - i - 1 <= max)
            .last()
        {
            s.drain(..=i);
        }
    }
}

// §4.6.4: ‘The following terms cause DNS queries: the "include", "a", "mx",
// "ptr", and "exists" mechanisms, and the "redirect" modifier. SPF
// implementations MUST limit the total number of those terms to 10 during SPF
// evaluation’
fn increment_lookup_count(query: &mut Query) -> EvalResult<()> {
    trace!(query, Tracepoint::IncrementLookupCount);
    query.state.increment_lookup_count(query.config.max_lookups())
}

// §4.6.4: ‘there may be cases where it is useful to limit the number of "terms"
// for which DNS queries return either a positive answer (RCODE 0) with an
// answer count of 0, or a "Name Error" (RCODE 3) answer. These are sometimes
// collectively referred to as "void lookups".’
pub fn increment_void_lookup_count_if_void(query: &mut Query, count: usize) -> EvalResult<()> {
    if count == 0 {
        trace!(query, Tracepoint::IncrementVoidLookupCount);
        query.state.increment_void_lookup_count(query.config.max_void_lookups())
    } else {
        Ok(())
    }
}

fn increment_per_mechanism_lookup_count(query: &mut Query, i: &mut usize) -> EvalResult<()> {
    trace!(query, Tracepoint::IncrementPerMechanismLookupCount);
    if *i < query.config.max_lookups() {
        *i += 1;
        Ok(())
    } else {
        Err(EvalError::PerMechanismLookupLimitExceeded)
    }
}

pub fn to_eval_result<T>(result: LookupResult<Vec<T>>) -> EvalResult<Vec<T>> {
    match result {
        Ok(r) => Ok(r),
        Err(e) => {
            match e {
                LookupError::Timeout => Err(EvalError::Timeout),
                // §5: ‘If the server returns "Name Error" (RCODE 3), then
                // evaluation of the mechanism continues as if the server
                // returned no error (RCODE 0) and zero answer records.’
                LookupError::NoRecords => Ok(Vec::new()),
                LookupError::Dns(e) => Err(EvalError::Dns(e)),
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::record::Ip4CidrLength;

    #[test]
    fn is_in_network_ok() {
        assert!(is_in_network(
            IpAddr::from([123, 12, 12, 12]),
            Some(Ip4CidrLength::new(24).unwrap()),
            IpAddr::from([123, 12, 12, 98]),
        ));
    }

    #[test]
    fn truncate_target_name_string_ok() {
        fn truncate<S: Into<String>>(s: S, max: usize) -> String {
            let mut s = s.into();
            truncate_target_name_string(&mut s, max);
            s
        }

        // Pathological case where final label longer than limit → no-op:
        assert_eq!(truncate("ab.cd.ef", 1), "ab.cd.ef");
        assert_eq!(truncate("ab.cd.ef.", 1), "ab.cd.ef");

        // Truncating:
        assert_eq!(truncate("ab.cd.ef", 2), "ef");
        assert_eq!(truncate("ab.cd.ef.", 2), "ef");
        assert_eq!(truncate("ab.cd.ef", 3), "ef");
        assert_eq!(truncate("ab.cd.ef", 4), "ef");
        assert_eq!(truncate("ab.cd.ef", 5), "cd.ef");
        assert_eq!(truncate("ab.cd.ef", 6), "cd.ef");
        assert_eq!(truncate("ab.cd.ef", 7), "cd.ef");
        assert_eq!(truncate("ab.cd.ef.", 7), "cd.ef");

        // Not longer than limit → no-op:
        assert_eq!(truncate("ab.cd.ef", 8), "ab.cd.ef");
        assert_eq!(truncate("ab.cd.ef.", 8), "ab.cd.ef");
        assert_eq!(truncate("ab.cd.ef", 9), "ab.cd.ef");
    }
}
