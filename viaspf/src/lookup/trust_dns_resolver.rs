// viaspf – implementation of the SPF specification
// Copyright © 2020–2022 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

use super::{Lookup, LookupError, LookupResult, Name};
use async_trait::async_trait;
use std::{
    error::Error,
    net::{IpAddr, Ipv4Addr, Ipv6Addr},
};
use trust_dns_resolver::{
    error::{ResolveError, ResolveErrorKind},
    Name as TrustDnsName, TokioAsyncResolver,
};

#[async_trait]
impl Lookup for TokioAsyncResolver {
    async fn lookup_a<'lookup, 'a>(&'lookup self, name: &'a Name) -> LookupResult<Vec<Ipv4Addr>> {
        Ok(self
            .ipv4_lookup(to_trust_dns_name(name)?)
            .await
            .map_err(to_lookup_error)?
            .into_iter()
            .collect())
    }

    async fn lookup_aaaa<'lookup, 'a>(&'lookup self, name: &'a Name) -> LookupResult<Vec<Ipv6Addr>> {
        Ok(self
            .ipv6_lookup(to_trust_dns_name(name)?)
            .await
            .map_err(to_lookup_error)?
            .into_iter()
            .collect())
    }

    async fn lookup_mx<'lookup, 'a>(&'lookup self, name: &'a Name) -> LookupResult<Vec<Name>> {
        let mut mxs = self
            .mx_lookup(to_trust_dns_name(name)?)
            .await
            .map_err(to_lookup_error)?
            .into_iter()
            .collect::<Vec<_>>();
        mxs.sort_by_key(|mx| mx.preference());
        mxs.into_iter()
            .map(|mx| Name::new(&mx.exchange().to_ascii()).map_err(wrap_error))
            .collect()
    }

    async fn lookup_txt<'lookup, 'a>(&'lookup self, name: &'a Name) -> LookupResult<Vec<String>> {
        Ok(self
            .txt_lookup(to_trust_dns_name(name)?)
            .await
            .map_err(to_lookup_error)?
            .into_iter()
            .map(|txt| {
                txt.iter()
                    .map(|data| String::from_utf8_lossy(data))
                    .collect()
            })
            .collect())
    }

    async fn lookup_ptr<'lookup>(&'lookup self, ip: IpAddr) -> LookupResult<Vec<Name>> {
        self.reverse_lookup(ip)
            .await
            .map_err(to_lookup_error)?
            .into_iter()
            .map(|name| Name::new(&name.to_ascii()).map_err(wrap_error))
            .collect()
    }
}

fn to_trust_dns_name(name: &Name) -> LookupResult<TrustDnsName> {
    TrustDnsName::from_ascii(name).map_err(wrap_error)
}

fn to_lookup_error(error: ResolveError) -> LookupError {
    match error.kind() {
        ResolveErrorKind::NoRecordsFound { .. } => LookupError::NoRecords,
        ResolveErrorKind::Timeout => LookupError::Timeout,
        _ => wrap_error(error),
    }
}

fn wrap_error(error: impl Error + Send + Sync + 'static) -> LookupError {
    LookupError::Dns(Some(error.into()))
}
