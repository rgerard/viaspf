// viaspf – implementation of the SPF specification
// Copyright © 2020–2022 David Bürgin <dbuergin@gluet.ch>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

use crate::lookup::Name;
use std::{
    error::Error,
    fmt::{self, Display, Formatter},
    hash::{Hash, Hasher},
    str::FromStr,
};

/// An error indicating that a query parameter could not be parsed.
#[derive(Clone, Copy, Debug, Default, Eq, Hash, PartialEq)]
pub struct ParseParamError;

impl Error for ParseParamError {}

impl Display for ParseParamError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "could not parse query parameter")
    }
}

/// A sender identity.
///
/// A `Sender` can be constructed from a MAIL FROM or HELO identity.
///
/// ```
/// use viaspf::Sender;
///
/// let mail_from_id = Sender::from_address("user@example.org")?;
/// let helo_id = Sender::from_domain("mail.example.org")?;
/// # Ok::<_, viaspf::ParseParamError>(())
/// ```
///
/// According to RFC 8616, internationalised domain names must be converted to
/// their ASCII representation before executing an SPF query. `Sender` can
/// handle both ASCII and Unicode-encoded inputs, and will in any case properly
/// apply the IDNA algorithm with Punycode encoding. Internationalised email
/// addresses and domain names may therefore be passed in directly in Unicode
/// format.
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Sender {
    local_part: String,
    domain_part: DomainName,
}

impl Sender {
    /// Creates a new `Sender` from either a MAIL FROM identity (an email
    /// address) or a HELO identity (a domain name).
    ///
    /// # Errors
    ///
    /// If the given string cannot be parsed, an error is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use viaspf::{DomainName, Sender};
    ///
    /// let sender = Sender::new("user@example.org")?;
    ///
    /// assert_eq!(sender.local_part(), "user");
    /// assert_eq!(sender.domain(), &DomainName::new("example.org")?);
    /// # Ok::<_, viaspf::ParseParamError>(())
    /// ```
    pub fn new(sender: &str) -> Result<Self, ParseParamError> {
        let (local_part, domain) = match sender.rsplit_once('@') {
            Some((local_part, domain)) => {
                if !is_local_part(local_part) {
                    return Err(ParseParamError);
                }
                (local_part, domain)
            }
            None => {
                // §4.3: ‘If the <sender> has no local-part, substitute the
                // string "postmaster" for the local-part.’
                ("postmaster", sender)
            }
        };

        domain.parse().map(|domain_part| Self {
            local_part: local_part.into(),
            domain_part,
        })
    }

    /// Creates a new `Sender` from an email address.
    ///
    /// # Errors
    ///
    /// If the given string is not a valid email address, an error is returned.
    pub fn from_address(s: &str) -> Result<Self, ParseParamError> {
        if s.rfind('@').is_none() {
            return Err(ParseParamError);
        }
        Self::new(s)
    }

    /// Creates a new `Sender` from a domain name.
    ///
    /// # Errors
    ///
    /// If the given string is not a valid domain name, an error is returned.
    pub fn from_domain(s: &str) -> Result<Self, ParseParamError> {
        if s.rfind('@').is_some() {
            return Err(ParseParamError);
        }
        Self::new(s)
    }

    /// Converts this sender into a tuple with its constituent parts.
    pub fn into_parts(self) -> (String, DomainName) {
        (self.local_part, self.domain_part)
    }

    /// Returns this sender’s local-part.
    pub fn local_part(&self) -> &str {
        &self.local_part
    }

    /// Returns this sender’s domain-part.
    pub fn domain(&self) -> &DomainName {
        &self.domain_part
    }
}

impl FromStr for Sender {
    type Err = ParseParamError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Self::from_address(s)
    }
}

impl Display for Sender {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}@{}", self.local_part, self.domain_part)
    }
}

// ‘local-part’ is defined in RFC 5321, §4.1.2. Modifications for
// internationalisation are in RFC 6531, §3.3. An older summary can be found in
// RFC 3696, §3.
fn is_local_part(s: &str) -> bool {
    // See RFC 5321, §4.5.3.1.1.
    if s.len() > 64 {
        return false;
    }

    if s.starts_with('"') {
        is_quoted_string(s)
    } else {
        is_dot_string(s)
    }
}

fn is_quoted_string(s: &str) -> bool {
    fn is_qtext_smtp(c: char) -> bool {
        c == ' ' || c.is_ascii_graphic() && !matches!(c, '"' | '\\') || !c.is_ascii()
    }

    if s.starts_with('"') && s.ends_with('"') && s.len() >= 2 {
        let mut quoted = false;
        for c in s[1..(s.len() - 1)].chars() {
            if quoted {
                if c == ' ' || c.is_ascii_graphic() {
                    quoted = false;
                } else {
                    return false;
                }
            } else if c == '\\' {
                quoted = true;
            } else if !is_qtext_smtp(c) {
                return false;
            }
        }
        !quoted
    } else {
        false
    }
}

fn is_dot_string(s: &str) -> bool {
    // See RFC 5322, §3.2.3, with the modifications in RFC 6531, §3.3.
    fn is_atext(c: char) -> bool {
        c.is_ascii_alphanumeric()
            || matches!(
                c,
                '!' | '#' | '$' | '%' | '&' | '\'' | '*' | '+' | '-' | '/' | '=' | '?' | '^' | '_'
                    | '`' | '{' | '|' | '}' | '~'
            )
            || !c.is_ascii()
    }

    let mut dot = true;
    for c in s.chars() {
        if dot {
            if is_atext(c) {
                dot = false;
            } else {
                return false;
            }
        } else if c == '.' {
            dot = true;
        } else if !is_atext(c) {
            return false;
        }
    }
    !dot
}

/// A syntactically valid, normalised domain name.
///
/// **`DomainName` is not a general purpose domain name implementation.** It is
/// a simplistic implementation that is sufficient for use as an SPF query
/// parameter.
#[derive(Clone, Debug)]
pub struct DomainName {
    name: Name,
}

impl DomainName {
    /// Constructs a syntactically valid, normalised domain name.
    ///
    /// In addition to checking label length restrictions and domain name
    /// syntax, this applies the IDNA algorithm with Punycode encoding to the
    /// given string. Unicode inputs are properly converted to yield a
    /// well-formed ASCII-encoded domain name.
    ///
    /// # Errors
    ///
    /// If the given string is not a valid domain name, an error is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use viaspf::DomainName;
    ///
    /// assert!(DomainName::new("example.org").is_ok());
    /// assert!(DomainName::new("_spf.example.org").is_err());
    ///
    /// assert_eq!(
    ///     DomainName::new("www.Bücher.de")?.to_string(),
    ///     "www.xn--bcher-kva.de"
    /// );
    /// # Ok::<_, viaspf::ParseParamError>(())
    /// ```
    pub fn new(s: &str) -> Result<Self, ParseParamError> {
        match Name::domain(s) {
            Ok(name) => Ok(Self { name }),
            Err(_) => Err(ParseParamError),
        }
    }

    /// Converts this domain name into a `Name`.
    pub fn into_name(self) -> Name {
        self.name
    }
}

impl FromStr for DomainName {
    type Err = ParseParamError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Self::new(s)
    }
}

impl Display for DomainName {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.name.fmt(f)
    }
}

impl AsRef<Name> for DomainName {
    fn as_ref(&self) -> &Name {
        &self.name
    }
}

impl PartialEq for DomainName {
    fn eq(&self, other: &Self) -> bool {
        self.name.eq(&other.name)
    }
}

impl Eq for DomainName {}

impl Hash for DomainName {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.name.hash(state);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sender_from_email_address() {
        let sender = Sender::from_address("amy@example.org").unwrap();
        assert_eq!(sender.local_part(), "amy");
        assert_eq!(sender.domain().to_string(), "example.org");
    }

    #[test]
    fn sender_from_domain() {
        let sender = Sender::from_domain("mail.example.org").unwrap();
        assert_eq!(sender.local_part(), "postmaster");
        assert_eq!(sender.domain().to_string(), "mail.example.org");
    }

    #[test]
    fn invalid_sender() {
        assert_eq!(Sender::new("amy@[12.34.56.78]"), Err(ParseParamError));

        // An empty MAIL FROM is invalid. Callers must themselves construct a
        // valid sender address in this case, see §2.4.
        assert_eq!(Sender::new(""), Err(ParseParamError));
    }

    #[test]
    fn is_local_part_ok() {
        assert!(is_local_part("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"));
        assert!(!is_local_part("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"));

        assert!(is_local_part(r#""""#));
        assert!(!is_local_part(r#"""#));
        assert!(is_local_part(r#""xy""#));
        assert!(is_local_part(r#""𝔵𝔶""#));
        assert!(is_local_part(r#""@""#));
        assert!(is_local_part(r#""x y""#));
        assert!(is_local_part(r#""\x""#));
        assert!(!is_local_part(r#""\𝔵""#));
        assert!(is_local_part(r#""\"""#));
        assert!(!is_local_part(r#""x\""#));
        assert!(!is_local_part(r#"""""#));

        assert!(!is_local_part(""));
        assert!(!is_local_part("."));
        assert!(!is_local_part(".x"));
        assert!(!is_local_part("x."));
        assert!(is_local_part("x"));
        assert!(is_local_part("xy"));
        assert!(is_local_part("xy.z"));
        assert!(is_local_part("xy.z+tag"));
        assert!(!is_local_part("xy..z"));
        assert!(is_local_part("겫13.12겫"));
    }

    #[test]
    fn is_local_part_rfc3696_ok() {
        // Examples from RFC 3696, §3 (with errata!).
        assert!(is_local_part(r#""Abc\@def""#));
        assert!(is_local_part(r#""Fred\ Bloggs""#));
        assert!(is_local_part(r#""Joe.\\Blow""#));
        assert!(is_local_part(r#""Abc@def""#));
        assert!(is_local_part(r#""Fred Bloggs""#));

        assert!(is_local_part("user+mailbox"));
        assert!(is_local_part("customer/department=shipping"));
        assert!(is_local_part("$A12345"));
        assert!(is_local_part("!def!xyz%abc"));
        assert!(is_local_part("_somename"));
    }
}
